### QnaProject

## Keywork
- RecyclerView
- Volley
- Coroutines

## Result
![01_qna-toolbar.jpg](./screenshots/01_qna-toolbar.jpg)
![02_qna-layout.jpg](./screenshots/02_qna-layout.jpg)
![03_qna_detail-toolbar&intent.jpg](./screenshots/03_qna_detail-toolbar&intent.jpg)
![04_qna-interface-connect.jpg](./screenshots/04_qna-interface-connect.jpg)
![05_qna_detail-interface-connect.jpg](./screenshots/05_qna_detail-interface-connect.jpg)
